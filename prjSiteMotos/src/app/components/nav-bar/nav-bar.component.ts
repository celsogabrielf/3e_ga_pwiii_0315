import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  actived: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  toggleMenu(){
    this.actived = !this.actived;
  }

  removeMenu(){
    this.actived = false;
  }

}
