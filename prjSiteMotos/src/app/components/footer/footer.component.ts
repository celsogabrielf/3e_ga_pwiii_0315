import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  actived1: boolean = false;

  actived2: boolean = false;

  actived3: boolean = false;

  actived4: boolean = false;

  actived5: boolean = false;


  constructor() { }

  ngOnInit(): void {
  }

  drawer1(){
    this.actived1 = !this.actived1;
  }

  drawer2(){
    this.actived2 = !this.actived2;
  }

  drawer3(){
    this.actived3 = !this.actived3;
  }

  drawer4(){
    this.actived4 = !this.actived4;
  }

  drawer5(){
    this.actived5 = !this.actived5;
  }

}
