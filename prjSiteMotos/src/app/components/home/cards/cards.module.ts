import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardsOneComponent } from './cards-one/cards-one.component';
import { CardsTwoComponent } from './cards-two/cards-two.component';

import {IvyCarouselModule} from 'angular-responsive-carousel';

@NgModule({
  declarations: [
    CardsOneComponent,
    CardsTwoComponent
  ],
  imports: [
    CommonModule,
    IvyCarouselModule
  ],
  exports: [
    CardsOneComponent,
    CardsTwoComponent
  ]
})
export class CardsModule { }
