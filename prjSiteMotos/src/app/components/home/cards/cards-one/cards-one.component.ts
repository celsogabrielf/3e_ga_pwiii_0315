import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import KeenSlider, { KeenSliderInstance } from "keen-slider";

@Component({
  selector: 'app-cards-one',
  templateUrl: './cards-one.component.html',
  styleUrls: [
    "./../../../../../../node_modules/keen-slider/keen-slider.min.css",
    './cards-one.component.css']
})
export class CardsOneComponent implements OnInit {

  cardsStreet = [

    {
      index: 0,
      cover: './../../../../../assets/images/cards__cards-one__CB1000R-Black.png',
      motorcycle: 'CB1000R',
      brand: 'honda'
    },
    {
      index: 1,
      cover: './../../../../../assets/images/cards__cards-one__CB500F.png',
      motorcycle: 'CB500F',
      brand: 'honda'
    },
    {
      index: 2,
      cover: './../../../../../assets/images/cards__cards-one__Fazer-FZ25.png',
      motorcycle: 'Fazer FZ25',
      brand: 'yamaha'
    },
    {
      index: 3,
      cover: './../../../../../assets/images/cards__cards-one__CG-160-Cargo.png',
      motorcycle: 'CG 160 Cargo',
      brand: 'honda'
    },
    {
      index: 4,
      cover: './../../../../../assets/images/cards__cards-one__CB-300.png',
      motorcycle: 'CB 300',
      brand: 'honda'
    },
    {
      index: 5,
      cover: './../../../../../assets/images/cards__cards-one__G-310-R.png',
      motorcycle: 'g 310 r',
      brand: 'bmw'
    }
  ]

  @ViewChild("sliderRefXl2") sliderRefXl2: ElementRef<HTMLElement>

  @ViewChild("sliderRefXl") sliderRefXl: ElementRef<HTMLElement>

  @ViewChild("sliderRefLg2") sliderRefLg2: ElementRef<HTMLElement>

  @ViewChild("sliderRefLg") sliderRefLg: ElementRef<HTMLElement>

  @ViewChild("sliderRefMd") sliderRefMd: ElementRef<HTMLElement>

  @ViewChild("sliderRefSm") sliderRefSm: ElementRef<HTMLElement>

  slider: null

  ngAfterViewInit() {
    this.slider = new KeenSlider(this.sliderRefXl2.nativeElement, {
      loop: true,
      slides: {
        perView: 4.5,
        spacing: 15,
      },
    }),
    this.slider = new KeenSlider(this.sliderRefXl.nativeElement, {
      loop: true,
      slides: {
        perView: 3.75,
        spacing: 15,
      },
    }),
    this.slider = new KeenSlider(this.sliderRefLg2.nativeElement, {
      loop: true,
      slides: {
        perView: 3.25,
        spacing: 15,
      },
    }),
    this.slider = new KeenSlider(this.sliderRefLg.nativeElement, {
      loop: true,
      slides: {
        perView: 2.75,
        spacing: 15,
      },
    }),
    this.slider = new KeenSlider(this.sliderRefMd.nativeElement, {
      loop: true,
      slides: {
        perView: 2.5,
        spacing: 15,
      },
    }),
    this.slider = new KeenSlider(this.sliderRefSm.nativeElement, {
      loop: true,
      slides: {
        perView: 1.75,
        spacing: 15,
      },
    })
  }


  constructor() { }

  ngOnInit(): void {
  }

}
