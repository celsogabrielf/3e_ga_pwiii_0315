import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import KeenSlider, { KeenSliderInstance } from "keen-slider";

@Component({
  selector: 'app-cards-two',
  templateUrl: './cards-two.component.html',
  styleUrls: [
    "./../../../../../../node_modules/keen-slider/keen-slider.min.css",
    './cards-two.component.css']
})
export class CardsTwoComponent implements OnInit {

  cardsScooter = [

    {
      index: 0,
      cover: './../../../../../assets/images/cards__cards-two__XMAX-ABS.png',
      motorcycle: 'XMAX ABS',
      brand: 'yamaha'
    },
    {
      index: 1,
      cover: './../../../../../assets/images/cards__cards-two__ADV.png',
      motorcycle: 'Honda ADV',
      brand: 'honda'
    },
    {
      index: 2,
      cover: './../../../../../assets/images/cards__cards-two__FLUO-ABS.png',
      motorcycle: 'FLUO ABS',
      brand: 'yamaha'
    },
    {
      index: 3,
      cover: './../../../../../assets/images/cards__cards-two__Biz-125.png',
      motorcycle: 'biz 125',
      brand: 'honda'
    },
    {
      index: 4,
      cover: './../../../../../assets/images/cards__cards-two__NMax-160.png',
      motorcycle: 'NMax 160',
      brand: 'Yamaha'
    },
    {
      index: 5,
      cover: './../../../../../assets/images/cards__cards-two__X-ADV.png',
      motorcycle: 'X ADV',
      brand: 'honda'
    }
  ]

  @ViewChild("sliderRefXl2") sliderRefXl2: ElementRef<HTMLElement>

  @ViewChild("sliderRefXl") sliderRefXl: ElementRef<HTMLElement>

  @ViewChild("sliderRefLg2") sliderRefLg2: ElementRef<HTMLElement>

  @ViewChild("sliderRefLg") sliderRefLg: ElementRef<HTMLElement>

  @ViewChild("sliderRefMd") sliderRefMd: ElementRef<HTMLElement>

  @ViewChild("sliderRefSm") sliderRefSm: ElementRef<HTMLElement>

  slider: null

  ngAfterViewInit() {
    this.slider = new KeenSlider(this.sliderRefXl2.nativeElement, {
      loop: true,
      slides: {
        perView: 4.5,
        spacing: 15,
      },
    }),
      this.slider = new KeenSlider(this.sliderRefXl.nativeElement, {
        loop: true,
        slides: {
          perView: 3.75,
          spacing: 15,
        },
      }),
      this.slider = new KeenSlider(this.sliderRefLg2.nativeElement, {
        loop: true,
        slides: {
          perView: 3.25,
          spacing: 15,
        },
      }),
      this.slider = new KeenSlider(this.sliderRefLg.nativeElement, {
        loop: true,
        slides: {
          perView: 2.75,
          spacing: 15,
        },
      }),
      this.slider = new KeenSlider(this.sliderRefMd.nativeElement, {
        loop: true,
        slides: {
          perView: 2.5,
          spacing: 15,
        },
      }),
      this.slider = new KeenSlider(this.sliderRefSm.nativeElement, {
        loop: true,
        slides: {
          perView: 1.75,
          spacing: 15,
        },
      })
  }

  constructor() { }

  ngOnInit(): void {
  }

}
