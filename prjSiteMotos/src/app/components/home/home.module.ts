import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SliderComponent } from './slider/slider.component';
import { HighlightsComponent } from './highlights/highlights.component';
import { HighlightsModule } from './highlights/highlights.module';
import { CardsComponent } from './cards/cards.component';
import { CardsModule } from './cards/cards.module';
import { AdditionalServicesComponent } from './additional-services/additional-services.component';



@NgModule({
  declarations: [
    SliderComponent,
    HighlightsComponent,
    CardsComponent,
    AdditionalServicesComponent
  ],
  imports: [
    CommonModule,
    HighlightsModule,
    CardsModule
  ],
  exports: [
    SliderComponent,
    HighlightsComponent,
    CardsComponent,
    AdditionalServicesComponent
  ]
})
export class HomeModule { }
