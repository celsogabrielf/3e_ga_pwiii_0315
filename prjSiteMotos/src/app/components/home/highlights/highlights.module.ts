import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SectionOneComponent } from './section-one/section-one.component';
import { SectionTwoComponent } from './section-two/section-two.component';



@NgModule({
  declarations: [
    SectionOneComponent,
    SectionTwoComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    SectionOneComponent,
    SectionTwoComponent
  ]
})
export class HighlightsModule { }
